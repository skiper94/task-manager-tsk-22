package ru.apolyakov.tm.exception.empty;

import ru.apolyakov.tm.exception.AbstractException;

public class EmptyDescriptionException extends AbstractException {

    public EmptyDescriptionException() {
        super("Error! Description is empty");
    }

}
