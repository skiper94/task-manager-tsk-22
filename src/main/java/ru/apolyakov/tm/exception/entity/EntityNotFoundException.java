package ru.apolyakov.tm.exception.entity;

import ru.apolyakov.tm.exception.AbstractException;

public class EntityNotFoundException extends AbstractException {

    public EntityNotFoundException() {
        super("Error! Entity not found...");
    }

}
