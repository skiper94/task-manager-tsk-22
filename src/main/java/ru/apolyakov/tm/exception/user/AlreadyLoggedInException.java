package ru.apolyakov.tm.exception.user;

import ru.apolyakov.tm.exception.AbstractException;

public class AlreadyLoggedInException extends AbstractException {

    public AlreadyLoggedInException() {
        super("Error! You are already logged in...");
    }

}
