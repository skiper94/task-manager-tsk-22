package ru.apolyakov.tm.exception.user;

import ru.apolyakov.tm.exception.AbstractException;

public class LoginExistsException extends AbstractException {

    public LoginExistsException(final String login) {
        super("Error! Login" + login + " already exists...");
    }

    public LoginExistsException() {
        super("Error! Login already exists...");
    }

}
