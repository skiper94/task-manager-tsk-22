package ru.apolyakov.tm.repository;

import ru.apolyakov.tm.api.repository.IProjectRepository;
import ru.apolyakov.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public Project findOneByIndex(final String userId, final Integer index) {
        return entities.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .skip(index - 1)
                .findFirst()
                .orElse(null);
    }

    @Override
    public Project findOneByName(final String userId, final String name) {
        return entities.stream()
                .filter(e -> userId.equals(e.getUserId()) && name.equals(e.getName()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void removeOneByIndex(final String userId, final Integer index) {
        remove(entities.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .skip(index - 1)
                .findFirst()
                .orElse(null)
        );
    }

    @Override
    public void removeOneByName(final String userId, final String name) {
        entities.remove(entities.stream()
                .filter(e -> userId.equals(e.getUserId()) && name.equals(e.getName()))
                .findFirst()
                .orElse(null)
        );
    }

}
