package ru.apolyakov.tm.enumerated;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETE("Complete");

    Status(final String dispayName) {
        this.dispayName = dispayName;
    }

    private String dispayName;

    public String getDispayName() {
        return dispayName;
    }

}
