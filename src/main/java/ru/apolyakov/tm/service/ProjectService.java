package ru.apolyakov.tm.service;

import ru.apolyakov.tm.enumerated.Status;
import ru.apolyakov.tm.api.repository.IProjectRepository;
import ru.apolyakov.tm.api.service.IProjectService;
import ru.apolyakov.tm.exception.empty.EmptyDescriptionException;
import ru.apolyakov.tm.exception.empty.EmptyIdException;
import ru.apolyakov.tm.exception.empty.EmptyNameException;
import ru.apolyakov.tm.exception.entity.ProjectNotFoundException;
import ru.apolyakov.tm.exception.system.ComparatorIncorrectException;
import ru.apolyakov.tm.exception.system.IndexIncorrectException;
import ru.apolyakov.tm.exception.system.StatusIncorrectException;
import ru.apolyakov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

import static ru.apolyakov.tm.util.CheckUtil.isEmpty;

public class ProjectService extends AbstractService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;


    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public Project add(final String userId, final String name, final String description) {
        if (isEmpty(name)) throw new EmptyNameException();
        if (isEmpty(description)) throw new EmptyDescriptionException();
        final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        return project;
    }

    @Override
    public void removeOneByName(final String userId, final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        projectRepository.removeOneByName(userId, name);
    }


    @Override
    public void removeProjectByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        projectRepository.removeOneByIndex(userId, index);
    }

    @Override
    public Project findOneByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return projectRepository.findOneByIndex(userId, index);
    }


    @Override
    public Project findOneByName(final String userId, final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        return projectRepository.findOneByName(userId, name);
    }


    @Override
    public Project updateProjectById(final String userId, final String id, final String name, final String description) {
        if (isEmpty(id)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectByName(final String userId, final String name, final String nameNew, final String description) {
        if (isEmpty(name)) throw new EmptyNameException();
        final Project project = findOneById(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(nameNew);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectByIndex(final String userId, final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (isEmpty(name)) throw new EmptyNameException();
        final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startProjectById(final String userId, final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByName(final String userId, final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        final Project project = findOneByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project finishProjectById(final String userId, final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project finishProjectByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project finishProjectByName(final String userId, final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        final Project project = findOneByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String id, final String userId, final Status status) {
        if (isEmpty(id)) throw new EmptyIdException();
        if (status == null) throw new StatusIncorrectException();
        final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final String userId, final Integer index, final Status status) {
        if (index == null) throw new IndexIncorrectException();
        if (status == null) throw new StatusIncorrectException();
        final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByName(final String userId, final String name, final Status status) {
        if (isEmpty(name)) throw new EmptyNameException();
        if (status == null) throw new StatusIncorrectException();
        final Project project = findOneByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

}
