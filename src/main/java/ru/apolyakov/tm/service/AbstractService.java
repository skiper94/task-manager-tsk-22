package ru.apolyakov.tm.service;

import ru.apolyakov.tm.api.IRepository;
import ru.apolyakov.tm.api.IService;
import ru.apolyakov.tm.exception.empty.EmptyIdException;
import ru.apolyakov.tm.exception.entity.EntityNotFoundException;
import ru.apolyakov.tm.exception.entity.ProjectNotFoundException;
import ru.apolyakov.tm.model.AbstractEntity;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import static ru.apolyakov.tm.util.CheckUtil.*;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    private final IRepository<E> repository;

    public AbstractService(IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public List<E> findAll(final String userId) {
        return repository.findAll(userId);
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        return repository.findAll(userId, comparator);
    }

    @Override
    public E findOneById(final String userId, final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return repository.findOneById(userId, id);
    }

    @Override
    public void clear(final String userId) {
        repository.clear(userId);
    }

    @Override
    public void removeOneById(final String userId, final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        repository.removeOneById(userId, id);
    }

    @Override
    public void add(final E e) {
        if (e == null) throw new EntityNotFoundException();
        repository.add(e);
    }

    @Override
    public void remove(final E e) {
        if (e == null) throw new EntityNotFoundException();
        repository.remove(e);
    }
}
