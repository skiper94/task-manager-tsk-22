package ru.apolyakov.tm.service;

import ru.apolyakov.tm.enumerated.Status;
import ru.apolyakov.tm.api.repository.ITaskRepository;
import ru.apolyakov.tm.api.service.ITaskService;
import ru.apolyakov.tm.exception.empty.EmptyDescriptionException;
import ru.apolyakov.tm.exception.empty.EmptyIdException;
import ru.apolyakov.tm.exception.empty.EmptyNameException;
import ru.apolyakov.tm.exception.entity.TaskNotFoundException;
import ru.apolyakov.tm.exception.system.ComparatorIncorrectException;
import ru.apolyakov.tm.exception.system.IndexIncorrectException;
import ru.apolyakov.tm.exception.system.StatusIncorrectException;
import ru.apolyakov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

import static ru.apolyakov.tm.util.CheckUtil.*;

public class TaskService extends AbstractService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public Task add(final String userId, final String name, final String description) {
        if (isEmpty(name)) throw new EmptyNameException();
        if (isEmpty(description)) throw new EmptyDescriptionException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        taskRepository.add(task);
        return task;
    }

    @Override
    public Task removeOneByName(final String userId, final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        return taskRepository.removeOneByName(name, userId);
    }

    @Override
    public void removeOneById(final String userId, final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        taskRepository.removeOneById(userId, id);
    }

    @Override
    public Task findOneById(final String userId, final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return taskRepository.findOneById(userId, id);
    }

    @Override
    public Task findOneByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return taskRepository.findOneByIndex(userId, index);
    }

    @Override
    public Task findOneByName(final String userId, final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        return taskRepository.findOneByName(userId, name);
    }

    @Override
    public Task updateTaskById(final String userId, final String id, final String name, final String description) {
        if (isEmpty(id)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task removeTaskByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return taskRepository.removeOneByIndex(userId, index);
    }

    @Override
    public Task updateTaskByIndex(final String userId, final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (isEmpty(name)) throw new EmptyNameException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskByName(final String userId, final String name, final String nameNew, final String description) {
        if (isEmpty(name) || isEmpty(nameNew)) throw new EmptyNameException();
        final Task task = findOneByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setName(nameNew);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startTaskById(final String userId, final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Task task = finishTaskByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByName(final String userId, final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        final Task task = finishTaskByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishTaskById(final String userId, final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByName(final String userId, final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        final Task task = findOneByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String userId, final String id, final Status status) {
        if (isEmpty(id)) throw new EmptyIdException();
        if (status == null) throw new StatusIncorrectException();
        final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final String userId, final Integer index, final Status status) {
        if (index == null) throw new IndexIncorrectException();
        if (status == null) throw new StatusIncorrectException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByName(final String userId, final String name, final Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new StatusIncorrectException();
        final Task task = findOneByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

}
