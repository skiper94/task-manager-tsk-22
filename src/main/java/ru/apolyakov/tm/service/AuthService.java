package ru.apolyakov.tm.service;

import ru.apolyakov.tm.api.service.IAuthService;
import ru.apolyakov.tm.api.service.IUserService;
import ru.apolyakov.tm.enumerated.Role;
import ru.apolyakov.tm.exception.user.AccessDeniedException;
import ru.apolyakov.tm.exception.user.EmptyLoginException;
import ru.apolyakov.tm.exception.user.EmptyPasswordException;
import ru.apolyakov.tm.model.User;
import ru.apolyakov.tm.util.HashUtil;

import static ru.apolyakov.tm.util.CheckUtil.isEmpty;

public class AuthService implements IAuthService {

    private IUserService userService;

    private String userId;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() {
        final String userId = getUserId();
        return userService.findById(userId);
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void login(final String login, final String password) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash()) || user.isLocked()) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void registry(final String login, final String password, final String email) {
        userService.create(login, password, email);
    }

    @Override
    public void checkRoles(Role... roles) {
        if (roles == null || roles.length == 0) return;
        final User user = getUser();
        if (user == null) throw new AccessDeniedException();
        final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (final Role item : roles) {
            if (item.equals(role)) return;
        }
        throw new AccessDeniedException();
    }

}
