package ru.apolyakov.tm.api;

import ru.apolyakov.tm.model.AbstractEntity;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void add(E entity);

    void clear(String userId);

    List<E> findAll(String userId, Comparator<E> comparator);

    List<E> findAll(String userId);

    E findOneById(String userId, String id);

    void removeOneById(String userId, String id);

    void remove(E e);

}
