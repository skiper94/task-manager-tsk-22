package ru.apolyakov.tm.api;

import ru.apolyakov.tm.model.AbstractEntity;
import ru.apolyakov.tm.model.Task;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IService<E extends AbstractEntity> {

    List<E> findAll(String userId);

    List<E> findAll(String userId, Comparator<E> comparator);

    E findOneById(String userId, String id);

    void clear(String userId);

    void add(final E entity);

    void removeOneById(String userId, String id);

    void remove(E entity);

}
