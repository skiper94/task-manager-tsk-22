package ru.apolyakov.tm.api.service;

import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    // TASK CONTROLLER
    List<Task> findAllTaskByProjectId(String userId, String projectId);

    // TASK CONTROLLER
    void bindTaskByProjectId(String userId, String projectId, String taskId);

    // TASK CONTROLLER
    Task unbindTaskFromProject(String userId, String projectId);

    // PROJECT CONTROLLER
    void removeTasksByProjectId(String userId, String projectId);

    void removeProjectById(String userId, String projectId);
}
