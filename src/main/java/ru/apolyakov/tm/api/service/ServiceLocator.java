package ru.apolyakov.tm.api.service;

public interface ServiceLocator {

    ITaskService getTaskService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

    IAuthService getAuthService();

    IUserService getUserService();

    ICommandService getCommandService();

}
