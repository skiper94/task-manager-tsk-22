package ru.apolyakov.tm.api.service;

import ru.apolyakov.tm.api.IService;
import ru.apolyakov.tm.enumerated.Status;
import ru.apolyakov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IService<Task> {

    Task add(String userId, String name, String description);

    Task findOneByName(String userId, String name);

    Task findOneByIndex(String userId, Integer index);

    Task updateTaskById(String userId, String id, String name, String description);

    Task updateTaskByIndex(String userId, Integer index, String name, String description);

    Task removeOneByName(String userId, String name);

    void removeOneById(String userId, String id);

    Task removeTaskByIndex(String userId, Integer index);

    Task startTaskById(String userId, String id);

    Task startTaskByIndex(String userId, Integer index);

    Task startTaskByName(String userId, String name);

    Task finishTaskById(String userId, String id);

    Task finishTaskByIndex(String userId, Integer index);

    Task finishTaskByName(String userId, String name);

    Task changeTaskStatusById(String userId, String id, Status status);

    Task changeTaskStatusByIndex(String userId, Integer index, Status status);

    Task changeTaskStatusByName(String userId, String name, Status status);

    Task updateTaskByName(String userId, String name, String nameNew, String description);

}
