package ru.apolyakov.tm.api.service;

import ru.apolyakov.tm.enumerated.Role;
import ru.apolyakov.tm.model.User;

import java.util.List;

public interface IUserService {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findById(String id);

    boolean existsByEmail(String email);

    boolean existsByLogin(String login);

    User findByLogin(String login);

    void removeByLogin(String userId, String login);

    void setPassword(String id, String password);

    User setRole(String userId, Role role);

    User updateUser(
            String id,
            String firstName,
            String lastName,
            String middleName
    );

    User lockedUserByLogin(String login);

    User unlockedUserByLogin(String login);

}
