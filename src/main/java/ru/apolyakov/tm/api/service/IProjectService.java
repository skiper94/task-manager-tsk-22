package ru.apolyakov.tm.api.service;

import ru.apolyakov.tm.api.IService;
import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.enumerated.Status;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IService<Project> {

    Project add(String userId, String name, String description);

    void removeOneByName(String userId, String name);

    Project findOneByIndex(String userId, Integer index);

    Project findOneByName(String userId, String name);

    Project updateProjectById(String userId, String id, String name, String description);

    void removeProjectByIndex(String userId, Integer index);

    Project updateProjectByIndex(String userId, Integer index, String name, String description);

    Project startProjectById(String userId, String id);

    Project startProjectByIndex(String userId, Integer index);

    Project startProjectByName(String userId, String name);

    Project finishProjectById(String userId, String id);

    Project finishProjectByIndex(String userId, Integer index);

    Project finishProjectByName(String userId, String name);

    Project changeProjectStatusById(String id, String userId, Status status);

    Project changeProjectStatusByIndex(String userId, Integer index, Status status);

    Project changeProjectStatusByName(String userId, String name, Status status);

    Project updateProjectByName(String userId, String name, String nameNew, String description);

}
