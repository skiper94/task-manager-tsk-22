package ru.apolyakov.tm.api.service;

import ru.apolyakov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    Collection<AbstractCommand> getCommands();

    Collection<AbstractCommand> getArguments();

    Collection<String> getListArgumentName();

    Collection<String> getListCommandName();

    void add(AbstractCommand command);

}
