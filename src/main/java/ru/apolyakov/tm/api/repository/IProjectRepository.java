package ru.apolyakov.tm.api.repository;

import ru.apolyakov.tm.api.IRepository;
import ru.apolyakov.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {

    Project findOneByIndex(String userId, Integer index);

    Project findOneByName(String userId, String name);

    void removeOneByIndex(String userId, Integer index);

    void removeOneByName(String userId, String name);

}
