package ru.apolyakov.tm.api.repository;

import ru.apolyakov.tm.api.IRepository;
import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    List<Task> findAllTaskByProjectId(String userId, String projectId);

    void removeAllTaskByProjectId(String userId, String projectId);

    void bindTaskByProject(String userId, String projectId, String taskId);

    Task unbindTaskByProjectId(String userId, String taskId);

    Task findOneByIndex(String userId, Integer index);

    Task findOneByName(String userId, String name);

    Task removeOneByIndex(String userId, Integer index);

    Task removeOneByName(String userId, String name);


}
