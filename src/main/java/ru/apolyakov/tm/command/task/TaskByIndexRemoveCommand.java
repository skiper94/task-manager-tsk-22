package ru.apolyakov.tm.command.task;

import ru.apolyakov.tm.exception.entity.TaskNotFoundException;
import ru.apolyakov.tm.model.Task;
import ru.apolyakov.tm.util.TerminalUtil;

public final class TaskByIndexRemoveCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-remove-by-index";
    }

    @Override
    public String description() {
        return "Remove task by index";
    }

    @Override
    public void execute() {
        System.out.println("[DELETE TASK]");
        System.out.println("ENTER INDEX");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = serviceLocator.getTaskService().removeTaskByIndex(serviceLocator.getAuthService().getUserId(), index);
        if (task == null) throw new TaskNotFoundException();
    }

}
