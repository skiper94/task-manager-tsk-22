package ru.apolyakov.tm.command.task;

import ru.apolyakov.tm.enumerated.Status;
import ru.apolyakov.tm.exception.entity.TaskNotFoundException;
import ru.apolyakov.tm.model.Task;
import ru.apolyakov.tm.util.TerminalUtil;

public final class TaskByIdSetStatusCommand extends AbstractTaskCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-set-status-by-id";
    }

    @Override
    public String description() {
        return "Set task status by id";
    }

    @Override
    public void execute() {
        System.out.println("[SETTING STATUS TO TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        System.out.println("to find the task id use the command: task-list");
        final String taskId = TerminalUtil.nextLine();
        System.out.println("ENTER NUMBER OF STATUS:");
        System.out.println("1: [NOT STARTED]");
        System.out.println("2: [IN PROGRESS]");
        System.out.println("3: [COMPLETE]");
        final Task task;
        final Integer statusNum = TerminalUtil.nextNumber();
        switch (statusNum) {
            case 1:
                task = serviceLocator.getTaskService().changeTaskStatusById(serviceLocator.getAuthService().getUserId(), taskId, Status.NOT_STARTED);
                break;
            case 2:
                task = serviceLocator.getTaskService().changeTaskStatusById(serviceLocator.getAuthService().getUserId(), taskId, Status.IN_PROGRESS);
                break;
            case 3:
                task = serviceLocator.getTaskService().changeTaskStatusById(serviceLocator.getAuthService().getUserId(), taskId, Status.COMPLETE);
                break;
            default:
                task = null;
        }
        if (task == null) throw new TaskNotFoundException();
    }
}
