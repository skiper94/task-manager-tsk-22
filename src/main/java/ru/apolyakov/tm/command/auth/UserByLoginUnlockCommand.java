package ru.apolyakov.tm.command.auth;

import ru.apolyakov.tm.enumerated.Role;

import static ru.apolyakov.tm.util.TerminalUtil.nextLine;

public final class UserByLoginUnlockCommand extends AbstractAuthCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Unlock user by login";
    }

    @Override
    public void execute() {
        System.out.println("[UNLOCK USER]");
        System.out.println("ENTER LOGIN:");
        serviceLocator.getUserService().unlockedUserByLogin(nextLine());
    }

    @Override
    public String name() {
        return "user-unlock-by-login";
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }
}
