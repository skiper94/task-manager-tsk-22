package ru.apolyakov.tm.command.auth;

import ru.apolyakov.tm.exception.user.EmailExistsException;
import ru.apolyakov.tm.exception.user.LoginExistsException;
import ru.apolyakov.tm.util.TerminalUtil;

public final class RegistryCommand extends AbstractAuthCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "registry";
    }

    @Override
    public String description() {
        return "Registry new user";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRATION NEW USER]");
        System.out.println("Enter login");
        final String login = TerminalUtil.nextLine();
        if (serviceLocator.getUserService().existsByLogin(login)) throw new LoginExistsException();
        System.out.println("Enter password");
        final String password = TerminalUtil.nextLine();
        System.out.println("Enter email");
        final String email = TerminalUtil.nextLine();
        if (serviceLocator.getUserService().existsByEmail(email)) throw new EmailExistsException();
        serviceLocator.getAuthService().registry(login, password, email);
    }
}
