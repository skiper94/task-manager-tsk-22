package ru.apolyakov.tm.command.auth;

import ru.apolyakov.tm.enumerated.Role;

import static ru.apolyakov.tm.util.TerminalUtil.nextLine;

public final class UserByLoginLockCommand extends AbstractAuthCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Lock user by login";
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN:");
        serviceLocator.getUserService().lockedUserByLogin(nextLine());
    }

    @Override
    public String name() {
        return "user-lock-by-login";
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }
}
