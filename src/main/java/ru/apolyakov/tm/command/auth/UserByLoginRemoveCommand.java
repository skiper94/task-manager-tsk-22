package ru.apolyakov.tm.command.auth;

import ru.apolyakov.tm.enumerated.Role;

import static ru.apolyakov.tm.util.TerminalUtil.nextLine;

public final class UserByLoginRemoveCommand extends AbstractAuthCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove user by login";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER LOGIN:");
        serviceLocator.getUserService().removeByLogin(serviceLocator.getAuthService().getUserId(), nextLine());
    }

    @Override
    public String name() {
        return "user-remove-by-login";
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }


}
