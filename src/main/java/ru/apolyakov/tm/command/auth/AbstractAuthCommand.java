package ru.apolyakov.tm.command.auth;

import ru.apolyakov.tm.command.AbstractCommand;
import ru.apolyakov.tm.exception.user.UserNotFoundException;
import ru.apolyakov.tm.model.User;

public abstract class AbstractAuthCommand extends AbstractCommand {

    protected void showUser(final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
        System.out.println(System.getProperty("line.separator"));
    }

}
