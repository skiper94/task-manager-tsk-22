package ru.apolyakov.tm.command.auth;

import ru.apolyakov.tm.enumerated.Role;

public final class LogoutCommand extends AbstractAuthCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String description() {
        return "Logout from the application";
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().logout();
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }
}
