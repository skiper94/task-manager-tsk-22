package ru.apolyakov.tm.command.system;

import ru.apolyakov.tm.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String name() {
        return "about";
    }

    @Override
    public String description() {
        return "Show developer info";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Andrey Polyakov");
        System.out.println("apolyakov@tsconsulting.com");
    }
}
