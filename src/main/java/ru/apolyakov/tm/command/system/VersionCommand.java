package ru.apolyakov.tm.command.system;

import ru.apolyakov.tm.command.AbstractCommand;

public final class VersionCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-v";
    }

    @Override
    public String name() {
        return "version";
    }

    @Override
    public String description() {
        return "Show version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("2.17.0");
    }

}
